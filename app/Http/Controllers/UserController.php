<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');

        if($search)
        {
            $users = User::where('name', 'like', '%' . $search . '%')
                ->get();
        }
        else
        {
            $users = User::all();
        }

        return view('users.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');
        $admin = $request->input('admin');
        $solde = $request->input('solde');



        $user = new User;
 
        $user->name = $name;
        $user->email = $email;
        $user->password = $password;
        $user->admin = $admin;
        $user->solde = $solde;
        $user->save();

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {


        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');
        $admin = $request->input('admin');
        $solde = $request->input('solde');


        $user = User::find($user->id);
        
        if(isset($name))
        {
            $user->name = $name;
        }
        
        if(isset($email))
        {
            $user->email = $email;
        }

        if(isset($password))
        {
            $user->password = $password;
        }

        if(isset($admin))
        {
            $user->admin = $admin;
        }
        
        if(isset($solde))
        {
            $user->solde = $solde;
        }
        $user->save();

        return redirect()->route('users.show',$user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user = User::find($user->id);
        $user->delete();
        
        return redirect()->route('users.index');
    }
}
