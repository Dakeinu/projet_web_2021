<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $orders = Order::all();


        return view('orders.index', ['orders' => $orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('orders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product_id = $request->input('product_id');

        $product = Product::find($product_id);
        $user_id = $request->input('user_id');
        $order = Order::order_exist($user_id,$product_id);

        if(isset($order[0])){
            $order = Order::find($order[0]->id);
            $quantity = $order->quantity + 1;
        }else{    
            $order = new Order;
            $quantity = 1;
        }
        $buy_at = date('Y-m-d');
        $buyed = false;
 
        $order->buy_at = $buy_at;
        $order->quantity = $quantity;
        $order->buyed = $buyed;
        $order->user_id = $user_id;
        $order->product_id = $product_id;

        $order->save();

        return redirect()->route('products.show',$product);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return view('orders.show', ['order' => $order]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        return view('orders.edit', ['order' => $order]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {

        $quantity = $request->input('quantity');
        $buyed = $request->input('buyed');

        $orders = Order::all();
        $total = 0;
        foreach($orders as $order){
            if($order->buyed == 0 && $order->user->id == Auth::user()->id){
                $total = $total + ($order->quantity * $order->product->price);
            }
        }
        foreach($orders as $order){
            if($order->buyed == 0 && $order->user->id == Auth::user()->id){
                if(isset($quantity))
                {
                    $order->quantity = $quantity;
                }

                if(isset($buyed) && Auth::user()->solde >= $total)
                {
                    $user = Auth::user();
                    $user-> solde = $user-> solde - $total;
                    Log::info($order);
                    $order->buyed = true;
                    Log::info($order);
                }

                $order->save();
            }
        }
        return redirect()->route('orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(order $order)
    {
        $order = Order::find($order->id);
        $order->delete();
        
        return redirect()->route('orders.index');
    }
}
