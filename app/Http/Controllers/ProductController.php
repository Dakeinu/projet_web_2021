<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Traits\UploadTrait;

class ProductController extends Controller
{
    use UploadTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');

        if($search)
        {
            $products = Product::where('gamename', 'like', '%' . $search . '%')
                ->get();
        }
        else
        {
            $products = Product::all();
        }

        return view('products.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $img = $request->file('img');
        $gamename = $request->input('gamename');
        $description = $request->input('description');
        $price = $request->input('price');



        $product = new Product;
 
        $product->gamename = $gamename;
        $product->description = $description;
        $product->price = $price;
        $product->save();
        if(isset($img)){
            $repertoire = "/images/";
            $nomImage = $product->id .".".$img->getClientOriginalExtension();
            $chemin = $repertoire . $nomImage;
            $product->image = $nomImage;
            $this->uploadOne($img, $repertoire, 'image', $nomImage);
        }
        $product->save();
        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show', ['product' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('products.edit', ['product' => $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {

        $gamename = $request->input('gamename');
        $description = $request->input('description');
        $price = $request->input('price');

        $image = $request->file('img');

        $vote = $request->input('vote');

        $product = Product::find($product->id);
        if(isset($vote)){
            $product->noter($vote);
        }
        if(isset($gamename))
        {
            $product->gamename = $gamename;
        }

        if(isset($price))
        {
            $product->price = $price;
        }

        if(isset($description))
        {
            $product->description = $description;
        }
        
        
        

        if(isset($image)){
            $repertoire = "/images/";
            $nomImage = $product->id .".".$image->getClientOriginalExtension();
            $chemin = $repertoire . $nomImage;
            $this->uploadOne($image, $repertoire, 'image', $nomImage);
            $product->image = $nomImage;
        }

        $product->save();



        return redirect()->route('products.show',$product);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product = Product::find($product->id);
        $product->delete();
        
        return redirect()->route('products.index');
    }
}
