<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use App\Product;
use App\User;


class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
            $comments = Comment::all();

        return view('comments.index', ['comments' => $comments]);
    }

    /**
     * Show the form for creating a new resource.
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
        $product = $request->input('product');
        return view('comments.create', ['product' => $product] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request 
     * @param  \App\Product  $product
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = $request->input('comment');
        $user = $request->input('user');
        $product_id = $request->input('product_id');
        $comment = new Comment;
        $comment->comment = $message;
        $comment->user_id = $user;
        $comment->product_id = $product_id;
        
        
        $comment->save();

        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        return view('comments.show', ['comment' => $comment]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        return view('comments.edit', ['comment' => $comment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {


        $comment = $request->input('comment');

        $comment = Comment::find($comment->id);
        
        $comment->comment = $comment;
        $comment->save();



        return redirect()->route('comments.show',$comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $comment = Comment::find($comment->id);
        $comment->delete();
        
        return redirect()->route('comments.index');
    }
}
