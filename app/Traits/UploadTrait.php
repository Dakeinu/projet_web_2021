<?php
namespace App\Traits;

use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait UploadTrait
{
    public function uploadOne(UploadedFile $uploadedFile, $folder = null, $disk = 'image', $filename = null)
    {

        $file = $uploadedFile->storeAs($folder, $filename, $disk);

        return $file;
    }
}
?>