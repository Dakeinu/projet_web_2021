<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Order extends Model
{
    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
    
    public static function order_exist($user_id,$product_id){
        return DB::table('orders')->where('user_id','=',$user_id)->where('product_id','=',$product_id)->where('buyed','<>',1)->get();
    }

    public static function orders_not_buyed($user_id){
        $orders = DB::table('orders')->where('user_id','=',$user_id)->where('buyed','<>',1)->get();
        Log::info($orders);
        return $orders;
    }


}
