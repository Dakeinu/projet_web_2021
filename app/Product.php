<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use App\Comment;
use Illuminate\Support\Facades\Log;

class Product extends Model
{
    public function comments(){
        return $this -> hasMany(Comment::class);
    }   
    public function orders(){
        return $this->belongsToMany(Order::class);
    }

    public function noter($note)
    {
        $this->nb_note = $this->nb_note + 1;
        $this->total_score = $this->total_score + $note ;
        $this->score = $this->total_score/$this->nb_note;
    }
}
  
    
    