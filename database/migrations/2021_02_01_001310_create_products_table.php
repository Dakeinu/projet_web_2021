<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('image', 1024)->nullable();
            $table->string('gamename', 255);
            $table->string('description', 255);
            $table->integer('price');
            $table->integer('score')->nullable();
            $table->integer('total_score')->default(0);
            $table->integer('nb_note')->default(0);
            $table->integer('activation_code')->nullable();
            $table->integer('sold')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
