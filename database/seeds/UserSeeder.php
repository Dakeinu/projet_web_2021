<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'admin',
                    'email' => 'admin@fake.mail',
                    'password' => '$2y$10$xNNFMX2..ClMeu3GbzrXpuOdnr4OxpBTJm5E/kI9xCJcM7xHSoGcu',
                    'admin' => true,
                    'solde' => 100,
                ],
            ]
        );
    }
}
