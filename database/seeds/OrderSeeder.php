<?php

use Illuminate\Database\Seeder;
use App\Order;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert(
            [
                [
                    'buy_at' => ('2021-02-19'),
                    'quantity' => 8,
                    'buyed' => true,
                    'product_id' => 1,
                    'user_id' => 1,
                ],
                [
                    'buy_at' => ('2019-03-30'),
                    'quantity' => 16,
                    'buyed' => false,
                    'product_id' => 2,
                    'user_id' => 1,
                ]
            ]
        );
    }
}
