<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(
            [
                [
                    'admin' => true,
                    'client' => true,
                ],
                [
                    'admin' => false,
                    'client' => true,
                ],
            ]
        );
    }
}
