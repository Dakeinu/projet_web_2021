<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(
            [
                [
                    'image' => 'rocket.jpg',
                    'gamename' => 'RocketLeague',
                    'description' => 'Jeux de foot',
                    'price' => 10,
                    'score' => 5,
                    'total_score' => 50,
                    'nb_note' => 10,
                    'activation_code' => 123456789,
                    'sold' => 50,
                ],
                [
                    'image' => 'r6.jpg',
                    'gamename' => 'R6',
                    'description' => 'Jeux de tir',
                    'price' => 20,
                    'score' => 5,
                    'total_score' => 50,
                    'nb_note' => 10,
                    'activationcode' => 987654321,
                    'sold' => 100,
                ],
                [
                    'image' => 'for-honor.jpg',
                    'gamename' => 'For Honor',
                    'description' => 'Jeux de combat',
                    'price' => 15,
                    'score' => 5,
                    'total_score' => 50,
                    'nb_note' => 10,
                    'activationcode' => 654987321,
                    'sold' => 150,
                ],
            ]
        );

        

    }
}
 