@extends('layouts/template')

@section('Titre')
    Users :
@endsection

@section('contain')
@if (Auth::user()->admin)
<div class="container">
    <table class="table table-bordered">

        @foreach ($users as $user) 
            <tr style="width: 18rem;">
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->admin }}</td>
                <td>{{ $user->solde }}</td>
                <td><a class="btn btn-primary" href="{{ route('users.show', $user) }}" class="btn btn-primary">Détails</a></td>
                <td><a class="btn btn-primary" href="{{ route('users.edit', $user) }}" class="btn btn-primary">Edit</a></td>
                <td><form action="{{ route('users.destroy', $user) }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button style="width:80%;padding:unset;font-size:1vw;height:1.5vw" type="submit" class="btn btn-danger">Delete</button>
                </form></td>
            </tr>
        @endforeach

    </table>
</div>
@endif
@if (!Auth::user()->admin)
    <p> Seul les administrateurs ont accès à cette page !</p>
    <div class="col-3">
        <a href="{{ route('products.index') }}" style="width:80%;padding:unset;font-size:1vw;height:1.5vw" class="btn btn-secondary">Retour</a>
    </div>
@endif
@endsection