@extends('layouts.template')

@section('Titre')
    Modifier :
@endsection

@section('contain')
<div class="container">
    <div class="row">
        <div class="card" style="margin: 0 auto;float: none;margin-bottom: 10px;padding:10px">
            <form method="POST" action="{{ route('users.update', $user) }}" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="row">
                    <div class="col-3">
                        <label style="font-size:1vw;" for="name">Nom :</label>
                    </div>
                    <div class="col-6">
                        <input style="font-size:1vw;" type="text" name="name" id="name" required="required" value="{{ $user->name }}"><br><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <label style="font-size:1vw;" for="description">Email :</label>
                    </div>
                    <div class="col-6">
                        <input style="font-size:1vw;" type="text" name="email" id="email" required="required" value="{{ $user->email }}"> </textarea><br><br>
                    </div>
                </div>

                @if (Auth::user()->admin)
                    <div class="row">
                        <div class="col-3">
                            <label style="font-size:1vw;" for="admin">Admin :</label>
                        </div>
                        <div class="col-6">
                            <input style="font-size:1vw;" type="text" name="admin" id="admin" required="required" value="{{ $user->admin }}"><br><br>
                        </div>

                        <div class="col-3">
                            <label style="font-size:1vw;" for="solde"> Wallet :</label>
                        </div>
                        <div class="col-6">
                            <input style="font-size:1vw;" type="text" name="solde" id="solde" required="required" value="{{ $user->solde }}"><br><br>
                        </div>
                    </div>
                @endif
                <input style="font-size:1vw;" type="submit" value="Edit" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>

@endsection