@extends('layouts.template')

@section('Titre')
    Creation :
@endsection

@section('contain')
@if (Auth::user()->admin)
<div class="container">
    <div class="row">
        <div class="card" style="margin: 0 auto;float: none;margin-bottom: 10px;padding:10px">
            <form method="POST" action="{{ route('users.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-3">
                        <label style="font-size:1vw;" for="name">Nom:</label>
                    </div>
                    <div class="col-6">
                        <input style="font-size:1vw;" type="text" name="name" id="name" required="required"><br><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <label style="font-size:1vw;" for="email">Email :</label>
                    </div>
                    <div class="col-6">
                        <input style="font-size:1vw;" type="text" name="email" id="email" required="required" ><br><br>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-3">
                            <label style="font-size:1vw;" for="admin">Admin :</label>
                        </div>
                        <div class="col-6">
                            <input style="font-size:1vw;" type="text" name="admin" id="admin" required="required"><br><br>
                        </div>

                        <div class="col-3">
                            <label style="font-size:1vw;" for="solde"> Wallet :</label>
                        </div>
                        <div class="col-6">
                            <input style="font-size:1vw;" type="text" name="solde" id="solde" required="required"><br><br>
                        </div>
                    </div>
                <input style="font-size:1vw;" type="submit" value="Ajouter" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
    
@endif

@if (!Auth::user()->admin)
    <p> Seul les administrateurs ont accès à cette page !</p>
    <div class="col-3">
        <a href="{{ route('products.index') }}" style="width:80%;padding:unset;font-size:1vw;height:1.5vw" class="btn btn-secondary">Retour</a>
    </div>
@endif
@endsection