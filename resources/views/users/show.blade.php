@extends('layouts.template')

@section('Titre')
    <p style="text-align:center">Compte</p>
@endsection

@section('contain')
<div class="container">
    <div class="row">
        <div class="col-9" style="border:groove; padding-bottom:0.5vw">
            <div class="row">
                <p style="text-align:center;width:100%;font-size:2vw">{{ $user->name }}</p>
                <p style="text-align:center;width:100%;font-size:2vw">{{ $user->email }}</p>
            </div>
            @if (Auth::user()->admin)
                <div class="row">
                    <p style="width:100%;font-size:2vw">Admin :</p>
                    <p style="width:100%;font-size:1.5vw">{{ $user->admin }}</p>
                </div>
            @endif       
        </div>
        
    </div>
    <div class="row" style="text-align:center">
        <div class="col-3">
            <a href="{{ route('users.index') }}" style="width:80%;padding:unset;font-size:1vw;height:1.5vw" class="btn btn-secondary">Retour</a>
        </div>
        @if (Auth::user()->admin)
            
            <div class="col-3">
                <form action="{{ route('users.destroy', $user) }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button style="width:80%;padding:unset;font-size:1vw;height:1.5vw" type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        @endif
        
        <div class="col-6">
                <a href="{{ route('users.edit', $user) }}" style="width:66%;padding:unset;font-size:1vw;height:1.5vw" class="btn btn-primary">Modifier</a>
        </div>
    </div>
</div>
<br>
@endsection
