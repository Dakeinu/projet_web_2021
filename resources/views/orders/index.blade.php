@extends('layouts.template')

@section('Titre')
    <p style="text-align:center">Liste des articles dans le panier</p>
@endsection

@section('contain')
<div class="container">
    <div class="row">

        @foreach ($orders as $order)
            @if($order->buyed == 0 && $order->user->id == Auth::user()->id)
                <div class="row" style="padding:1vw">
                    <div class="col-1">
                        <img src="/images/{{ $order->product->image }}" alt="" style="width:5vw;height:8vw"></a>
                    </div>
                    <div class="col-3" style="border:groove; padding-bottom:0.5vw">
                        <p style="text-align:center;width:100%;font-size:2vw">{{ $order->product->gamename }}</p>
                        <br>
                        <p style="text-align:center;width:100%;font-size:1vw">{{ $order->product->description }}</p>
                    </div>
                    <div class="col-1" style="border:groove; padding-bottom:0.5vw">
                        <p style="text-align:center;width:100%;font-size:2vw">{{ $order->product->price }} €</p>
                    </div>
                    <div class="col-4" style="border:groove; padding-bottom:0.5vw">
                        <form method="POST" action="{{ route('orders.update', $order) }}">
                            @method('PUT')
                            @csrf
                            <div class="row">
                                <div class="col-6">
                                    <input style="font-size:1vw" type="number" name="quantity" id="quantity" value="{{ $order->quantity}}"  min="0" required="required" ><br><br>
                                </div>
                                <div class="col-6">
                                    <input style="font-size:1vw;" type="submit" value="valider" class="btn btn-primary">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-2" style="border:groove; padding-bottom:0.5vw">
                    <form action="{{ route('orders.destroy', $order) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button style="width:80%;padding:unset;font-size:1vw;height:1.5vw" type="submit" class="btn btn-danger">Delete</button>
                    </form>
                    </div>
                </div>
              </div>
            @endif
        @endforeach
    </div>
    <a class="btn btn-primary" href="{{ route('products.index') }}" class="btn btn-primary">Retour</a>
    <form method="POST" action="{{ route('orders.update', $orders[0]) }}">
                @method('PUT')
                @csrf
                <input type="hidden" name="buyed" id="buyed" value="{{$orders[0]}}">
                <input style="font-size:1vw;" type="submit" value="Acheter" name="Acheter" id="Acheter" class="btn btn-primary">
    </form>
</div>
@endsection