@extends('layouts.template')

@section('Titre')
    Creation :
@endsection

@section('contain')
@if (Auth::user()->admin)
<div class="container">
    <div class="row">
        <div class="card" style="margin: 0 auto;float: none;margin-bottom: 10px;padding:10px">
            <form method="POST" action="{{ route('products.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-3">
                        <label style="font-size:1vw;" for="img">Image:</label>
                    </div>
                    <div class="col-6">
                        <input style="font-size:1vw;" type="file" name="img" id="img" required="required"><br><br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-3">
                        <label style="font-size:1vw;" for="gamename">Nom:</label>
                    </div>
                    <div class="col-6">
                        <input style="font-size:1vw;" type="text" name="gamename" id="gamename" required="required"><br><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <label style="font-size:1vw;" for="description">Description:</label>
                    </div>
                    <div class="col-6">
                        <textarea style="font-size:1vw;" rows="4" cols="40" name="description" id="description" required="required" > </textarea><br><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <label style="font-size:1vw;" for="price">Prix:</label>
                    </div>
                    <div class="col-6">
                        <input style="font-size:1vw;" type="text" name="price" id="price" required="required"><br><br>
                    </div>
                </div>
                <input style="font-size:1vw;" type="submit" value="Ajouter" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
@endif

@if (!Auth::user()->admin)
    <p> Seul les administrateurs ont accès à cette page !</p>
    <div class="col-3">
        <a href="{{ route('products.index') }}" style="width:80%;padding:unset;font-size:1vw;height:1.5vw" class="btn btn-secondary">Retour</a>
    </div>
@endif

@endsection