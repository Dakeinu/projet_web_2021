@extends('layouts.template')

@section('Titre')
    <p style="text-align:center">Modifier</p>
@endsection

@section('contain')
<div class="container">
    <div class="row">
        <div class="card" style="margin: 0 auto;float: none;margin-bottom: 10px;padding:10px">
            <form method="POST" action="{{ route('products.update', $product) }}" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="row">
                    <div class="col-3">
                        <label style="font-size:1vw;" for="img">Image:</label>
                    </div>
                    <div class="col-6">
                        <input style="font-size:1vw;" type="file" name="img" id="img"  value="{{ $product->image }}"><br><br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-3">
                        <label style="font-size:1vw;" for="gamename">Nom:</label>
                    </div>
                    <div class="col-6">
                        <input style="font-size:1vw;" type="text" name="gamename" id="gamename" required="required" value="{{ $product->gamename }}"><br><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <label style="font-size:1vw;" for="description">Description:</label>
                    </div>
                    <div class="col-6">
                        <textarea style="font-size:1vw;" rows="4" cols="40" name="description" id="description" required="required" value="{{ $product->description }}"> {{ $product->description }}</textarea><br><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <label style="font-size:1vw;" for="price">Prix:</label>
                    </div>
                    <div class="col-6">
                        <input style="font-size:1vw;" type="text" name="price" id="price" required="required" value="{{ $product->price }}"><br><br>
                    </div>
                </div>
                <input style="font-size:1vw;" type="submit" value="Modifier" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
@endsection
