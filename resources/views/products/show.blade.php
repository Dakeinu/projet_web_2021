@extends('layouts.template')

@section('Titre')
    <p style="text-align:center">Détails</p>
@endsection

@section('contain')
<div class="container">
    <div class="row" style="padding:1vw">
        <div class="col-3">
            <img style="width:100%" src="/images/{{ $product->image }}" alt=""></a>
        </div>
        <div class="col-9" style="border:groove; padding-bottom:0.5vw">
            <div class="row">
                <p style="text-align:center;width:100%;font-size:2vw">{{ $product->gamename }}</p>
            </div>
            <div class="row">
                <p style="width:100%;font-size:2vw">Description :</p>
                <p style="width:100%;font-size:1.5vw">{{ $product->description }}</p>
            </div>
            <div class="row">
                <div class="col-2">
                    <p style="width:100%;font-size:1vw">{{ $product->score }}/5</p>
                </div>
                <div class="col-5">
                    <p style="width:100%;font-size:1vw">Exemplaire vendu : {{ $product->sold }}</p>
                </div>
                <div class="col-5">
                    <form method="POST" action="{{ route('orders.store', ['product_id'=>$product, 'user_id' => Auth::user()->id]) }}">
                        @csrf
                        <input style="font-size:1vw;" type="submit" value="Ajouter au panier : {{ $product->price }}€" class="btn btn-primary" style="width:80%;padding:unset;font-size:1vw;height:1.5vw">
                    </form>
                </div>
            </div>
        </div>
        
    </div>
    <div class="row" style="text-align:center; margin-bottom:1vw">
        <div class="col-3">
            <a href="{{ route('products.index') }}" style="width:80%;padding:unset;font-size:1vw;height:1.5vw" class="btn btn-secondary">Retour</a>
        </div>
        @if (Auth::user()->admin)
            <div class="col-6">
                <a href="{{ route('products.edit', $product) }}" style="width:66%;padding:unset;font-size:1vw;height:1.5vw" class="btn btn-primary">Modifier</a>
            </div>
            <div class="col-3">
                <form action="{{ route('products.destroy', $product) }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button style="width:80%;padding:unset;font-size:1vw;height:1.5vw" type="submit" class="btn btn-danger">Delete</button>
                </form>
            </div>
        @endif
    </div>
    <div class="row" style="text-align:center; margin-bottom:1vw">
        <div class="col-3">
            <a href="{{ route('comments.create',['product' => $product]) }}" style="width:80%;padding:unset;font-size:1vw;height:1.5vw" class="btn btn-primary">Ajouter un commentaire</a>
        </div>
        <div class="col-3">
            <form method="POST" action="{{ route('products.update', $product) }}">
                @method('PUT')
                @csrf
                <div class="row">
                    <div class="col-3">
                        <label style="font-size:1vw;" for="vote">Note:</label>
                    </div>
                    <div class="col-6">
                        <input style="font-size:1vw;" type="number" name="vote" id="vote"  min="0" max="5" required="required" ><br><br>
                    </div>
                </div>
                <input style="font-size:1vw;" type="submit" value="Voter" class="btn btn-primary">
            </form>
        </div>
    </div>
    <p>Liste des commentaires</p>
    <div class="row" style="border:groove;border-bottom:unset">
        @foreach ($product->comments as $comment)
            <div class="row">
                <div class="col-2" style="border-right:groove;border-bottom:groove">
                    <p>Utilisateur : {{ $comment->user->name }}</p>
                </div>
                <div class="col-10" style="border-bottom:groove">
                    <p>{{ $comment->comment }}</p>
                </div>
            </div><br>
        @endforeach
    </div>
</div>
<br>
@endsection
