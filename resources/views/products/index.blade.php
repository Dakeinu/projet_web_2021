@extends('layouts/template')

@section('Title')
    GamePrestige
@endsection

@section('contain')
<div class="container">
    <div class="row">

        @foreach ($products as $product) 
            <div class="card" style="width: 18rem;">
                <img src="/images/{{ $product->image }}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">{{ $product->gamename }}</h5>
                  <p class="card-text">{{ $product->price }}€</p>
                  <a class="btn btn-primary" href="{{ route('products.show', $product) }}" class="btn btn-primary">Détails</a>
                </div>
              </div>
        @endforeach

    </div>
  </div>
@endsection