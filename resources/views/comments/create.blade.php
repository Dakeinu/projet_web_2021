@extends('layouts.template')

@section('Titre')
    Creation :
@endsection

@section('contain')
<div class="container">
    <div class="row">
        <div class="card" style="margin: 0 auto;float: none;margin-bottom: 10px;padding:10px">
            <form method="POST" action="{{ route('comments.store', ['product_id' => $product, 'user' => Auth::user()]) }}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-3">
                        <label style="font-size:1vw;" for="comment">Commentaire :</label>
                    </div>
                    <div class="col-6">
                        <input style="font-size:1vw;" type="text" name="comment" id="comment" required="required"><br><br>
                    </div>
                </div>
                <input style="font-size:1vw;" type="submit" value="Ajouter" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
@endsection